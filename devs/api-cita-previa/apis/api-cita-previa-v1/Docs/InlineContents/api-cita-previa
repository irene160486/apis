## API Reference

# api-cita-previa-gaudi

## POSTMAN
✨MAquesta és la col·lecció Postman amb la qual et permet poder obtenir mes informació sobre com executar les diferents operacions del servei de cita prèvia.✨M

> Note:  [![Executar a Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/3445ba39e2ab6a3656f7)

API Version: v

Servei de gestió de cites prèvies amb les operacions disponibles de:

```
Recuperar la cita prèvia
Recuperar Temes
Obtenir dies lliures
Recuperar dades de l'oficina
Recuperar Oficina
Gravar nova cita
Eliminar cita
Recuperar dades del tema
Sol·licitud d'hores lliures
```
CONTACT

NAME: Pablo Rábago
EMAIL: prabago@minsait.com


## INDEX

- 1. CONSULTA
- 1.1 POST /recuperarDadesTema
- 1.2 POST /recuperarTemes
- 1.3 POST /recuperarDadesOficina
- 1.4 POST /recuperarOficines
- 1.5 POST /solicitarHoresLliures
- 1.6 POST /retornarCitaPrevia
- 1.7 POST /obtenirDiesLliures
- 2. CREACIÓ
- 2.1 POST /gravarNovaCita
- 3. ESBORRAT
- 3.1 POST /eliminarCita


Security and Authentication

SECURITY SCHEMES

### KEY TYPE DESCRIPTION

default oauth


## API

## 1. CONSULTA

## 1.1 POST /recuperarDadesTema

recuperarDadesTema

Operació per a l'obtenció de les dades associat al tema enviat en la petició

REQUEST

```
REQUEST BODY - application/json
{
Envelope* {
Body* {
recuperarDadesTema* string min:1 chars
DEFAULT:SUDO
}
}
}
```
RESPONSE

```
STATUS CODE - 200: Exemple de respostes segons la dada consultada
```
```
RESPONSE MODEL - application/json
{
Envelope* {
Body* {
recuperarDadesTemaResponse* {
codiError* number
dataFi* string min:1 chars
dataInici* string min:1 chars
texteComentari undefined
texteDocumentacio undefined
}
}
}
}
```
## 1.2 POST /recuperarTemes

recuperarTemes

Operació per a l'obtenció dels temes disponibles.No requereix que sigui informat valor cap en la
sol·licitud.

REQUEST

```
REQUEST BODY - application/json
{
Envelope* {
```

```
Body* {
recuperarTemes* string min:1 chars
}
}
}
```
RESPONSE

```
STATUS CODE - 200: Exemple de respostes segons la dada consultada
```
```
RESPONSE MODEL - application/json
{
Envelope* {
Body* {
recuperarTemesResponse* {
item* [{
Array of object:
codigo* string min:1 chars
descripcion* string min:1 chars
textoCompetencia* string
}]
}
}
}
}
```
## 1.3 POST /recuperarDadesOficina

recuperarDadesOficina

Operació per a l'obtenció de les dades associat al codi d'oficina enviat en la petició

REQUEST

```
REQUEST BODY - application/json
{
Envelope* {
Body* {
recuperarDadesOficina* string min:1 chars
}
}
}
```
RESPONSE

### STATUS CODE - 200: OK

```
RESPONSE MODEL - application/json
{
Envelope* {
Body* {
recuperarDadesOficinaResponse* {
adreca* string min:1 chars
codiError* number
codiPostal* number
email* string min:1 chars
```

```
idCentro* string min:1 chars
idOficina* number
municipi* string min:1 chars
nomOficina* string min:1 chars
telefon* string min:1 chars
tipoOficina* string min:1 chars
}
}
}
}
```
## 1.4 POST /recuperarOficines

recuperarOficines

Operació que recupera les dades d'oficina associats a un determinat tema

REQUEST

```
REQUEST BODY - application/json
{
Envelope* {
Body* {
recuperarOficines* string min:1 chars
}
}
}
```
RESPONSE

### STATUS CODE - 200: OK

```
RESPONSE MODEL - application/json
{
Envelope* {
Body* {
recuperarOficinesResponse* {
item* [{
Array of object:
codPostal* number
codigo* string min:1 chars
descripcion* string min:1 chars
direccion* string min:1 chars
municipio* string min:1 chars
}]
}
}
}
}
```
## 1.5 POST /solicitarHoresLliures

solicitarHoresLliures

Operació per a la sol·licitud d'hores disponibles per a un determinat tema i oficina


REQUEST

```
REQUEST BODY - application/json
{
Envelope* {
Body* {
solicitarHoresLliures* {
codigoTema* string min:1 chars
dataDemanada* string min:1 chars
idOficina* string
}
}
}
}
```
RESPONSE

### STATUS CODE - 200: OK

```
RESPONSE MODEL - application/json
{
Envelope* {
Body* {
solicitarHoresLliuresResponse* {
codiError* number
horesLliures* string
idSequencia* number
}
}
}
}
```
## 1.6 POST /retornarCitaPrevia

retornarCitaPrevia

Operació que retorna el detall d'una cita

REQUEST

```
REQUEST BODY - application/json
{
Envelope* {
Body* {
retornarCitaPrevia* string min:1 chars
}
}
}
```
RESPONSE

### STATUS CODE - 200: OK

```
RESPONSE MODEL - application/json
{
}
```

## 1.7 POST /obtenirDiesLliures

obtenirDiesLliures

Operació que permet poder obtenir els dies lliure per a una determinada oficina, tema, mes i any.

REQUEST

```
REQUEST BODY - application/json
{
Envelope* {
Body* {
obtenirDiesLliures* {
anyo* string min:1 chars
codTema* string min:1 chars
idOficina* string min:1 chars
mes* string min:1 chars
}
}
}
}
```
RESPONSE

### STATUS CODE - 200: OK

```
RESPONSE MODEL - application/json
{
Envelope* {
Body* {
obtenirDiesLliuresResponse* {
codiError* number
diesLliures* {
GTH30_DiaLliure* [{
Array of object:
numDia* number
tipusDia* string min:1 chars
}]
}
}
}
}
}
```

## 2. CREACIÓ

## 2.1 POST /gravarNovaCita

gravarNovaCita

Operació que permet poder gravar una nova cita.

REQUEST

```
REQUEST BODY - application/json
{
Envelope* {
Body* {
gravarNovaCita* {
EMail* string min:1 chars
codTema* string min:1 chars
dataHora* string min:1 chars
idFiscal* string min:1 chars
idOficina* string
idSequencia* number
movil* string
nombre* string min:1 chars
origen* boolean
}
}
}
}
```
RESPONSE

### STATUS CODE - 200: OK

```
RESPONSE MODEL - application/json
{
Envelope* {
Body* {
gravarNovaCitaResponse* {
codiError* number
descMesa* string
idCita* string
}
}
}
}
```

## 3. ESBORRAT

## 3.1 POST /eliminarCita

eliminarCita

Operació que permet poder eliminar una cita.

REQUEST

```
REQUEST BODY - application/json
{
Envelope* {
Header* string
Body* {
eliminarCita* {
idCita* string min:1 chars
usuari* boolean
}
}
}
}
```
RESPONSE

### STATUS CODE - 200: OK

```
RESPONSE MODEL - application/json
{
Envelope* {
Body* {
eliminarCitaResponse* string min:1 chars
}
}
}
```

